# CIA
Communities In Atria Platform

### setup node and react
```
 npm run setup
```
### To start deployment server
at [localhost](http://localhost:8000/)
```
 npm start 
```

### To start react dev server
```
 npm run start-react
```
### Build react app
```
 npm run build 
```
